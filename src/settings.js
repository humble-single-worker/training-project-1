module.exports = {

  title: '新冠疫情可视化平台',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  fixedHeader: false,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  //网页logo
  sidebarLogo: true,
}
