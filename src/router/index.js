import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: '首页' }
    }]
  },  
  
  {
    path: '/dynamic',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Dynamic',
        component: () => import('@/views/dynamic/index'),//加载的页面组件
        meta: { title: '疫情动态', icon: 'form' }
      }
    ]
  },  
  
  {
    path: '/map',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Map',
        component: () => import('@/views/map/index'),//加载的页面组件
        meta: { title: '疫情地图', icon: '地图' }
      }
    ]
  },      
  
  {
    path: '/echarts',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Echarts',
        //加载的页面
        component: () => import('@/views/echarts/index'),
        meta: { title: '实时疫情', icon: '图表' }
      }
    ]
  },
  
  // {
  //   path: '/location',
  //   component: Layout,
  //   children: [
  //     {
  //       path: 'index',
  //       name: 'Location',
  //       component: () => import('@/views/location/index'),//加载的页面组件
  //       meta: { title: '定点地图', icon: '地图3' }
  //     }
  //   ]
  // },  
  
  {
    path: '/hos',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Hos',
        component: () => import('@/views/hos/index'),//加载的页面组件
        meta: { title: '医院位置', icon: '地图2' }
      },      
      {
        path: 'detail',
        name: 'Detail',
        component: () => import('@/views/location/index'),//加载的页面组件
        meta: { title: '医院位置', icon: '地图2' },
        hidden: true,
      }
    ]
  },

  {
    path: '/data_list',
    component: Layout,
    redirect: '/data_list/table',
    name: 'Data_list',
    meta: { title: '疫情数据', icon: 'el-icon-s-help' },
    children: [
      {
        path: 'table',
        name: 'Table',
        component: () => import('@/views/dynamic_data/DataList'),
        meta: { title: '数据列表', icon: 'table' }
      },
      {
        path: 'update',
        name: 'Update',
        component: () => import('@/views/dynamic_data/DateUpdate'),
        meta: { title: '数据上报', icon: 'tree' }
      },      
      {
        path: 'datasearch',
        name: 'Datasearch',
        component: () => import('@/views/dynamic_data/DataSearch'),
        meta: { title: '数据查询', icon: 'tree' }
      },
    ]
  }, 
  
  {
    path: '/manager',
    component: Layout,
    redirect: '/manager/article',
    name: 'Manager',
    meta: { title: '疫情管理', icon: '疫情填报' },
    children: [
      {
        path: 'article',
        name: 'Article',
        component: () => import('@/views/manager/article'),
        meta: { title: '新闻资讯', icon: '新闻' }
      },
      {
        path: 'category',
        name: 'Category',
        component: () => import('@/views/manager/category'),
        meta: { title: '咨询分类', icon: '资讯' }
      },           
      {
        path: 'hospital',
        name: 'Hospital',
        component: () => import('@/views/manager/hospital'),
        meta: { title: '定点医院', icon: '医院' }
      },      

    ]
  },
  {
    path: '/wuzi',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Wuzi',
        component: () => import('@/views/wuzi/index'),//加载的页面组件
        meta: { title: '物资拨放', icon: '我的订单' }
      },
      {
        path: 'index2',
        name: 'Wuzi_2',
        component: () => import('@/views/wuzi/index2'),//加载的页面组件
        meta: { title: '表单生成', icon: '我的订单' },
        hidden: true,
      }
    ]
  },   

  {
    path: '/全局疫情',
    component: Layout,
    children: [
      {
        path: 'index',
        name: '全局疫情',
        component: () => import('@/views/全局疫情/index'),//加载的页面组件
        meta: { title: '全局疫情', icon: '全局_运营' }
      }
    ]
  }, 
  {
    path: '/allhos',
    component: Layout,
    children: [{
      path: 'allhos',
      name: 'Allhos',
      component: () => import('@/views/allhos/index'),
      meta: { title: '医院位置显示', icon: 'tree' }
    }]
  }, 

  {
    path: '/worldData',
    component: Layout,
    children: [{
      path: 'worldData',
      name: 'WorldData',
      component: () => import('@/views/worldData/index'),
      meta: { title: '世界疫情数据', icon: 'tree' }
    }]
  },  
  {
    path: '/province_data',
    component: Layout,
    children: [{
      path: 'province_data',
      name: 'Province_data',
      component: () => import('@/views/province_data/index'),
      meta: { title: '各省疫情数据', icon: 'tree' }
    }]
  },  
  {
    path: '/ee_all',
    component: Layout,
    children: [
      {
        path: 'See_all',
        name: 'see_all',
        //加载的页面
        component: () => import('@/views/See_all/index'),
        meta: { title: '疫情总览', icon: '图表' }
      }
    ]
  },
  {
    path: '/contrast',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Contrast',
        component: () => import('@/views/contrast/index'),
        meta: { title: '疫情对比', icon: 'form' }
      },
      {
        path: 'worldmap',
        name: 'Worldmap',
        component: () => import('@/views/contrast/map'),
        hidden:true,
      },
    ]
  },
  
  {
    path: '/nested',
    component: Layout,
    redirect: '/nested/menu1',
    name: 'Nested',
    meta: {
      title: 'Nested',
      icon: 'nested'
    },
    children: [
      {
        path: 'menu1',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'Menu1',
        meta: { title: 'Menu1' },
        children: [
          {
            path: 'menu1-1',
            component: () => import('@/views/nested/menu1/menu1-1'),
            name: 'Menu1-1',
            meta: { title: 'Menu1-1' }
          },
          {
            path: 'menu1-2',
            component: () => import('@/views/nested/menu1/menu1-2'),
            name: 'Menu1-2',
            meta: { title: 'Menu1-2' },
            children: [
              {
                path: 'menu1-2-1',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
                name: 'Menu1-2-1',
                meta: { title: 'Menu1-2-1' }
              },
              {
                path: 'menu1-2-2',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
                name: 'Menu1-2-2',
                meta: { title: 'Menu1-2-2' }
              }
            ]
          },
          {
            path: 'menu1-3',
            component: () => import('@/views/nested/menu1/menu1-3'),
            name: 'Menu1-3',
            meta: { title: 'Menu1-3' }
          }
        ]
      },
      {
        path: 'menu2',
        component: () => import('@/views/nested/menu2/index'),
        name: 'Menu2',
        meta: { title: 'menu2' }
      }
    ]
  },

 
  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
