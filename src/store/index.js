import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import dataList from './modules/dataList'
import dataUpdate from './modules/dataUpdate'
import category  from './modules/category'

// 新状态机先创建文件然后导入，
import article from './modules/article'
import hospital from './modules/hospital'
import dataSearch from './modules/dataSearch'

// import all_text from './modules/全局疫情'




Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    category,
    app,
    settings,
    user,
    dataList,
    dataUpdate,
    article,
    hospital,
    dataSearch,
    // all_text,
  },
  getters
})

export default store