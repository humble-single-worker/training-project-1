
import { post,get } from '../../http/axios'

export default {
    namespaced:true,
    state:{
        dataList:{},
    },
    mutations:{
        set_dataList(state, dataList) {//dataList才是我们的结果
            state.dataList = dataList;
        }
    },
    actions:{
        async saveOrupdate(context,parmas) {
            let res = await post('/epidemic/saveOrUpdate',parmas);
            // console.log(res);
            return res;
        },
        async page_query(context, params) {
            let res = await get('/epidemic/pageQuery', params);
            // console.log(res);
            
            context.commit('set_dataList', res.data)//提交突变，需要两个变量名称和参数变量值
        }
    }
}

