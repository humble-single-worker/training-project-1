import {get } from '../../http/axios'

export default {
    // 命名空间
    namespaced: true,
    // 状态->data
    state: {
        dataSearch: {}
    },
    // 过滤器->处理state中的变量
    getters: {},
    // 突变->改变state中变量值的唯一方式
    mutations: {
        set_dataSearch(state, dataSearch) {
            state.dataSearch = dataSearch;
        }
    },
    // 动作->methods，发送请求
    actions: {
        async page_query(context, params) {
            let res = await get('/epidemic/pageQuery', params);
            console.log(res.data.list);
            // 提交突变中的方法
            context.commit('set_dataSearch', res.data)
        }
    },
}