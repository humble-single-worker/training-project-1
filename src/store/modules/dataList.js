import { get } from '../../http/axios'
//dataList的状态机
//1.先得到数据
//2.然后提交突变
//3.然后突变修改state
export default {
    // 命名空间
    namespaced: true,
    // 状态 -> data
    state: {
        dataList: {},
    },
    // 过滤器 -> 处理state中的变量
    getters: {
        confirmed(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.confirmed;
                });
                return res;
            }
        },        
        suspected(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.suspected;
                });
                return res;
            }
        },          
        dead(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.dead;
                });
                return res;
            }
        },          
        cure(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.cure;
                });
                return res;
            }
        },           
        severe(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.severe;
                });
                return res;
            }
        },           
        outside(state){
            if(state.dataList.list){
                var res = 0;
                // console.log(state.dataList.list)
                state.dataList.list.forEach((item)=>{
                    res += item.outside;
                });
                return res;
            }
        },        

    },
    // 突变 -> 改变state中变量的唯一方式
    mutations: {
        set_dataList(state, dataList) {//dataList才是我们的结果
            state.dataList = dataList;
        }
    },
    // 动作 -> 等价于methods，发送请求
    actions: {
        
        async page_query(context, params) {
            let res = await get('/epidemic/pageQuery', params);
            // console.log(res.data,'123');

            context.commit('set_dataList', res.data)//提交突变，需要两个变量名称和参数变量值
        }
    },


}