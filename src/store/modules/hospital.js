
import { post,get } from '../../http/axios'

export default {
    namespaced:true,
    state:{
        hos:{}
    },
    mutations:{
        // 当数据需要在vue文件中调用时需给全局变量state赋值
        // 突变名(state,传入值)
        // state变量赋值为val
        set_hos(state,val){
            state.hos = val;
        }
    },
    actions:{
        // 提交突变时需要在传参处给一个{commit}
        async findHos({commit},params){
            let res = await get('/hospital/pageQuery',params);
            // 当网页反馈值需要在网页中显示的时候需要提交突变
            // commit('突变名','传入值')
            commit('set_hos',res.data);
            // console.log(res.data);
            return res.data;
        },
        async saveHos({commit},params){
            let res = await　post('/hospital/saveOrUpdate',params);
            return res;
        },  
        async deleteHos(content,params){
            let res = await get('/hospital/deleteById',params)
            return res;
        },

    }
}

