import { get,post } from '../../http/axios'

export default {
    // 命名空间
    namespaced:true,
    // 状态 -> data
    state:{
        article:{},
        category:{}
    },
    // 突变 -> 改变state中变量值的唯一方式
    mutations:{
        set_article(state,val){
            state.article = val;
        },
        set_category(state,val){
            state.category = val;
        }
    },
    // 动作 -> methods，发送请求
    actions:{
        async page_query_article({commit},params) {
            let res = await get('/article/pageQuery',params);
            // 调用突变中的方法
            commit('set_article',res.data);
        },
        async saveOrUpdate({commit},params) {
            let res = await post('/article/saveOrUpdate',params);
            return res;
        },
        async findAllCategory({commit}) {
            let res = await get('/category/findAll');
            commit('set_category',res.data);
        },
        async deleteArticle({commit},params){
            let res = await get('/article/deleteById',params);
            console.log(res);
            return res;
        },
        async FindById({commit},params){
            let res = await get('/article/findById',params);
            return res;
        }
    }
}