//1 要把状态机引入到index里面，然后再到modules里面写入状态机
import { get, post } from '../../http/axios'

export default {
    // 命名空间
    namespaced: true,
    // 状态 -> data
    state: {
        category:[],
    },
    // 过滤器 -> 处理state中的变量
    // getters: {

    // },
    // 突变 -> 改变state中变量值的唯一方式
    mutations: {
        //接收两个参数
        set_category(state,val){
            state.category = val;
        },
    },
    // 动作 -> methods，发送请求
    actions:{
        // 提交突变时需要在传参处给一个{commit}
        async findCate({commit},params){
            let res = await get('/category/findAll',params);
            // 当网页反馈值需要在网页中显示的时候需要提交突变
            // commit('突变名','传入值')
            commit('set_category',res);
            // console.log(res,'11');
            return res;
        },
        async saveCate(content,params){
            let res = await　post('/category/saveOrUpdate',params);
            // console.log(res,'====');
            return res;
        },  
        async deleteCate(content,params){
            let res = await get('/category/deleteById',params)
            return res;
        },

    }
}